<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<link rel="icon" href="https://saint-lukes.com/wp-content/uploads/2020/08/Asset-13.png">
    <title>Home</title>
  </head>
	
	
<body>
<?php
// Initialize the session
session_start();
 
// Check if the user is logged in, if not then redirect to login page
if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true) {
    header("location: login.php");
    exit;
}
?>

	
<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <div class="container">
  <a class="navbar-brand" href="index.php">SLEC Digital Music Library</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNav">
    <ul class="navbar-nav">
      <li class="nav-item active">
        <a class="nav-link" href="index.php">Home<span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="items.php">All Items</a>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Resources
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="https://www.lectionarypage.net/" target="_blank">Lecionary Page</a>
          <a class="dropdown-item" href="https://www.hymnary.org" target="_blank">Hymnary</a>        
        </div>
		<li>
				<a href="includes/logout.php">
					<button type="button" class="btn btn-outline-dark">Sign Out</button>
				</a>
	    </li>
      </li>
    </ul>
  </div>
  </div>
</nav>
	
	

<div class="container mt-4"><h1>Search.</h1></div>
<div class="container">
	<div class="row border bg-light p-5">
		<div class="col-md-12">
		  <form action="includes/search.inc.php" method="get">
			  <div class="input-group" id="adv-search">
                <input type="text" class="form-control" name="search" placeholder="Search">
				  <div class="input-group-btn">
					<div class="btn-group" role="group">
						<select name="column" class="custom-select">
							<option value="a.title, a.composer, a.date, a.medium, a.publisher, a.call_no">Keyword</option>
							<option value="a.title">Title</option>
							<option value="a.composer">Composer</option>
							<option value="a.date">Date</option>
							<option value="b.season">Liturgical season</option>
							<option value="a.medium">Med. of performance</option>
							<option value="a.call_no">Call No.</option>
							<option value="a.publisher">Publisher</option>
							<option value="c.rights">Rights</option>
					  </select>
					  <button type="submit" class="btn btn-primary">Search</button>
					</div>
				  </div>
				</div>
			  </form>
			</div>	
 		 </div>
	  <div class="row mt-2">
    <div class="container text-center p-2">
		<a href="insert.php">
			<button type="button" class="btn btn-outline-success">Add a record</button>
		</a>
	</div>
</div>
	
	
	
</div>
</div>

 
	  
	  
	  
	  

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>
</html>